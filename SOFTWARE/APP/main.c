/******************** (C) COPYRIGHT 2008 STMicroelectronics ********************
* File Name          : main.c
* Author             : whq
* Version            : V2.0.1
* Date               : 20/02/2013
* Description        : Main program body
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/** @addtogroup IAP
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "bsp.h"
#include "cortexm3_macro.h"
#include "flash_opt.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/




/*******************************************************************************
* Function Name  : main
* Description    : Main program.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/

struct FW_CONFIG {
	unsigned char version;
	unsigned char version_name[32];
	unsigned char mode;
	unsigned int size;
	unsigned char checksum[64];
	unsigned char date[32];
	
	unsigned char flag;
	unsigned int mask; //0xFAFBFDFE
};



int main(void)
{
	struct FLASH_CONFIG_DATA fwconfig;
	
	BspInit();
	MyPrintf("\r\n\r\n");
	MyPrintf("\r\n\r\n");
	MyPrintf("+------------------------------------+\r\n");
	MyPrintf("|                                    |\r\n");
	MyPrintf("|         EasyIO Bootloader          |\r\n");
	MyPrintf("|                                    |\r\n");
	MyPrintf("+------------------------------------+\r\n");
	MyPrintf("\r\n\r\n");
	
	
	MyPrintf("Bootloader    : 0x%08X - %04d kbytes\r\n",BOOTLOADER_ADDR,BOOTLOADER_SIZE/1024);
	MyPrintf("Config        : 0x%08X - %04d kbytes\r\n",CONFIG_ADDR,CONFIG_SIZE/1024);
	MyPrintf("App           : 0x%08X - %04d kbytes\r\n",APP_ADDR,APP_SIZE/1024);
	MyPrintf("Download      : 0x%08X - %04d kbytes\r\n",DOWN_ADDR,DOWNLOAD_SIZE/1024);
	MyPrintf("\r\n\r\n");
	
	
//	snprintf(fwconfig.version_name,32,"sdfadfa");
	memcpy(&fwconfig,(u32*)(CONFIG_ADDR),sizeof(struct FLASH_CONFIG_DATA));
	
	if ((strstr(fwconfig.name,FW_CONFIG_NAME) == 0) || (fwconfig.clen != sizeof(struct FLASH_CONFIG_DATA)) || (fwconfig.caddr != CONFIG_ADDR))
	{
		//�ظ���������
		memset(&fwconfig,sizeof(struct FLASH_CONFIG_DATA),0x0);
		snprintf(fwconfig.name,16,FW_CONFIG_NAME);
		fwconfig.clen = sizeof(struct FLASH_CONFIG_DATA);
		fwconfig.caddr = CONFIG_ADDR;
		
		FLASH_ProgramStart(CONFIG_ADDR,CONFIG_SIZE);
		FLASH_AppendBuffer((u8*)(&fwconfig),sizeof(struct FLASH_CONFIG_DATA));
		FLASH_AppendEnd();
		FLASH_ProgramDone();
		//
	}
	
	MyPrintf("Config Name : %s\r\n",fwconfig.name);
	MyPrintf("Config Code : %d\r\n",fwconfig.update_code);
	MyPrintf("Fw     Size : %d\r\n",fwconfig.fw_size);
	MyPrintf("\r\n\r\n");
	
	//fwconfig.update_code = 1;
	if (fwconfig.update_code == 0)
	{
	}else
	if (fwconfig.update_code == 1)
	{
		
		MyPrintf("Copy Fw from 0x%08x to 0x%08x\r\n",DOWN_ADDR,APP_ADDR);
		
		FLASH_ProgramStart(APP_ADDR,APP_SIZE);
		FLASH_AppendBuffer((u8*)(DOWN_ADDR),APP_SIZE);
		FLASH_AppendEnd();
		FLASH_ProgramDone();
		
		fwconfig.update_code = 0;
		FLASH_ProgramStart(CONFIG_ADDR,CONFIG_SIZE);
		FLASH_AppendBuffer((u8*)(&fwconfig),sizeof(struct FLASH_CONFIG_DATA));
		FLASH_AppendEnd();
		FLASH_ProgramDone();
		
		
		
		
		//
	}
	
	MyPrintf("EasyIO@RTThread Running 0x%08x\r\n",APP_ADDR);
	MyPrintf("\r\n\r\n");
	JumpToApp(APP_ADDR);
	
//	
//	Print("000\r\n");
//	Print((u8*)fwconfig.version_name);
//	Print("\r\n");
	
//	FLASH_ProgramStart()
	
//	FLASH_ProgramStart(CONFIG_ADDR,(1024*(8+0)));
//	FLASH_AppendBuffer((u8*)(&fwconfig),sizeof(fwconfig));
//	FLASH_AppendEnd();
//	FLASH_ProgramDone();
//	
//	memset(&fwconfig,0x0,sizeof(fwconfig));
//	
//	memcpy(&fwconfig,(u32*)(CONFIG_ADDR),sizeof(fwconfig));
//	Print("1111\r\n");
//	Print((u8*)fwconfig.version_name);
//	Print("\r\n");
	
	
//	Print("Backup fw \r\n");
//	
//	FLASH_ProgramStart(DOWN_ADDR,(1024*(200)));
//	FLASH_AppendBuffer((u8*)(APP_ADDR),1024*200);
//	FLASH_AppendEnd();
//	FLASH_ProgramDone();
	
//	Print("Copy fw \r\n");
//	FLASH_ProgramStart(APP_ADDR,(1024*(200)));
//	FLASH_AppendBuffer((u8*)(DOWN_ADDR),1024*200);
//	FLASH_AppendEnd();
//	FLASH_ProgramDone();
//	Print("Copy fw finish \r\n");
//	
	
	Print("start app ...\r\n");
	JumpToApp(APP_ADDR);
	Print("start app err ...\r\n");
	return 0;
}

#ifdef  DEBUG
/*******************************************************************************
* Function Name  : assert_failed
* Description    : Reports the name of the source file and the source line number
*                  where the assert_param error has occurred.
* Input          : - file: pointer to the source file name
*                  - line: assert_param error line source number
* Output         : None
* Return         : None
*******************************************************************************/
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif





/******************* (C) COPYRIGHT 2008 STMicroelectronics *****END OF FILE****/
