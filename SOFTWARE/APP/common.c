/*-------------------------------------------------------------------------

                            接口部分
                            
                            
-------------------------------------------------------------------------*/

#include <string.h>

#include "stm32f10x_flash.h"
#include "common.h"
#include "bsp.h"



/*******************************************************************************
* Function Name :static void Print(u8 *str)
* Description   :打印消息  串口发送
* Input         :
* Output        :
* Other         :
* Date          :2013.03.01
*******************************************************************************/
void Print(u8 *str)
{
    u16 len = 0;

    len = strlen((const char *)str);

    while (BspUsart1Send(str, len) != TRUE);
}
#include <stdarg.h>
void MyPrintf(const char *fmt, ...)
{
	#define RT_CONSOLEBUF_SIZE 128
	va_list args;
  unsigned int  length;
  static char rt_log_buf[RT_CONSOLEBUF_SIZE];

  va_start(args, fmt);
  length = vsnprintf(rt_log_buf, sizeof(rt_log_buf) - 1, fmt, args);
  if (length > RT_CONSOLEBUF_SIZE - 1)
        length = RT_CONSOLEBUF_SIZE - 1;
		
	Print((u8*)rt_log_buf);
  va_end(args);
}


/*******************************************************************************
* Function Name :static void TimEndHandle(void)
* Description   :接收字符超时回调函数
* Input         :
* Output        :
* Other         :
* Date          :2013.02.19
*******************************************************************************/
static void TimEndHandle(void)
{
    BspTim3Close();

}



/*******************************************************************************
* Function Name :void JumpToApp(void)
* Description   :跳转到应用程序区
* Input         :None
* Output        :None
* Other         :None
* Date          :2013.02.19

*******************************************************************************/

typedef  void (*pFunction)(void);
void JumpToApp(u32 appAddr)
{
	pFunction JumpToApplication;
	u32 JumpAddress;
  BspClose();
	JumpAddress = *(u32*) (appAddr + 4);
	JumpToApplication = (pFunction)JumpAddress;
  /* Initialize user application's Stack Pointer */
  __MSR_MSP(*(vu32*) appAddr);
  JumpToApplication();
}

#if 0
static void JumpToApp(void)
{
    if (((*(vu32*)ApplicationAddress) & 0x2FFE0000 ) == 0x20000000)
    { 
        BspClose();
        /* Jump to user application */
        m_JumpAddress = *(vu32*) (ApplicationAddress + 4);
        JumpToApplication = (FunVoidType) m_JumpAddress;

        /* Initialize user application's Stack Pointer */
        __MSR_MSP(*(vu32*) ApplicationAddress);
        JumpToApplication();
    }
}

#endif

/*******************************************************************************
* Function Name :void CommonInit(void)
* Description   :接口初始化
* Input         :
* Output        :
* Other         :
* Date          :2013.02.19
*******************************************************************************/
void CommonInit(void)
{
    //BspTim3SetIRQCallBack(TimEndHandle);
    //BspUsart1IRQCallBack(ReceOneChar);
}

/*********************************** END **************************************/

