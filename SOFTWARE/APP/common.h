/*-------------------------------------------------------------------------

                            接口头文件
                            
-------------------------------------------------------------------------*/



#ifndef _COMMON_H_
#define _COMMON_H_

#include "stm32f10x_type.h"
#include "../../../../easyio_core/ota_http_tcp.h"



//#define ApplicationAddress      0x8004000       //APP程序首地址
//#define ApplicationSize         1024*8          //程序预留空间
#define STM32F10X_HD                            //所选择芯片为大容量芯片
//#define ApplicationSize			((*(vu16 *)(0x1FFFF7E0)) * 1000) //芯片存储空间


#if defined (STM32F10X_MD) || defined (STM32F10X_MD_VL)
 #define PAGE_SIZE                         (0x400)    /* 1 Kbyte */
 #define FLASH_SIZE                        (0x20000)  /* 128 KBytes */
#elif defined STM32F10X_CL
 #define PAGE_SIZE                         (0x800)    /* 2 Kbytes */
 #define FLASH_SIZE                        (0x40000)  /* 256 KBytes */
#elif defined STM32F10X_HD
 #define PAGE_SIZE                         (0x800)    /* 2 Kbytes */
 #define FLASH_SIZE                        (0x80000)  /* 512 KBytes */
#elif defined STM32F10X_XL
 #define PAGE_SIZE                         (0x800)    /* 2 Kbytes */
 #define FLASH_SIZE                        (0x100000) /* 1 MByte */
#else 
 #error "Please select first the STM32 device"    
#endif

void Print(u8 *str);
void MyPrintf(const char *fmt, ...);
void CommonInit(void);
void JumpToApp(u32 appAddr);

#endif
/*********************************** END **************************************/

