#ifndef __flash_opt__
#define __flash_opt__

#include "common.h"



void FLASH_ProgramStart(u32 addr , u32 size);
u32 FLASH_AppendOneByte(u8 Data);
u32 FLASH_AppendBuffer(u8 *Data , u32 size);
void FLASH_AppendEnd(void);
u32 FLASH_WriteBank(u8 *pData, u32 addr, u16 size);
void FLASH_ProgramDone(void);


int __write_flash(u32 StartAddr,u16 *buf,u16 len);
int __read_flash(u32 StartAddr,u16 *buf,u16 len);

#endif